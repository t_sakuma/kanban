import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import controllers.services._
import models._
import play.api.libs.json._
import scala.concurrent._
import scala.concurrent.duration._
import org.joda.time.DateTime
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class ExpenseSpec extends PlaySpec {
  "リクエストから支出に変換" should {
    "リクエスト正常" should {
      "支出を返す" in {
        // 前提
        val request = Json.obj(
          "accountId" -> "xxx",
          "money" -> 1000,
          "timestamp" -> 1483755835
        )
        val f = Future {
          ExpenseConverter.fromRequest(request)
        }
        Await.result(f, Duration.Inf) mustBe Expense(
          ExpenseId(AccountId("xxx"), new DateTime(1483755835)),
          1000
        )
      }
    }

    "リクエストが空" should {
      "リクエスト空エラーを返す" in {
        // 前提
        val request = Json.obj()
        val f = Future {
          ExpenseConverter.fromRequest(request)
        } map {
          _ => "エラーにならない"
        } recover {
          case e => Json.parse(e.getMessage)
        }
        Await.result(f, Duration.Inf) mustBe Json.parse("""{"result" : "JsError", "msg" : {
          "obj.money":[{"msg":["error.path.missing"],"args":[]}],
          "obj.timestamp":[{"msg":["error.path.missing"],"args":[]}],
          "obj.accountId":[{"msg":["error.path.missing"],"args":[]}]
        }}""")
      }
    }
  }

  "支出追加" should {
    val expense = Expense(
      ExpenseId(AccountId("xxx"), new DateTime(1483755835)),
      1000
    )
    val account = Option(Account(AccountId("xxx")))
    "家計簿が存在する" should {
      "エラーを出力しない" in {
        val f = ExpenseFlow.insert(
          expense,
          (accountId: AccountId) => Future(account),
          (expense: Expense) => Future(())
        ) map {
          _ => "失敗しない"
        } recover {
          case e => e.getMessage
        }
        Await.result(f, Duration.Inf) mustBe "失敗しない"
      }
    }

    "家計簿が存在しない" should {
      "家計簿不明エラーを返す" in {
        val f = ExpenseFlow.insert(
          expense,
          (accountId: AccountId) => Future(None),
          (expense: Expense) => Future(())
        ) map {
          _ => "失敗しない"
        } recover {
          case e => e.getMessage
        }
        Await.result(f, Duration.Inf) mustBe "家計簿を取得できません"
      }
    }
  }
}
