import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import controllers.services._
import models._
import play.api.libs.json._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ApplicationSpec extends PlaySpec with OneAppPerTest {

  "Todo" should {
    "index convert" in  {
      TodoConverter.toIndexData(Seq.empty[Todo]) mustBe Json.parse("[]")
      TodoConverter.toIndexData(Seq[Todo](Todo("id1", "title1"))) mustBe Json.parse("""[
        {"id":"id1","title":"title1","status":"do"}
        ]""")
      TodoConverter.toIndexData(Seq[Todo](Todo("id1", "title1"), Todo("id2", "title2"))) mustBe Json.parse("""[
        {"id":"id1","title":"title1","status":"do"},
        {"id":"id2","title":"title2","status":"do"}
        ]""")
    }
    "set status" in  {
      Todo("id1", "title1").status mustBe Todo.DO
      Todo("id1", "title1").setDone().status mustBe Todo.DONE
      Todo("id1", "title1").setDoing().status mustBe Todo.DOING
      Todo("id1", "title1").setDoing().setDo().status mustBe Todo.DO
    }
  }

  // "HomeController" should {
  //
  //   "render the index page" in {
  //     val home = route(app, FakeRequest(GET, "/")).get
  //
  //     status(home) mustBe OK
  //     contentType(home) mustBe Some("text/html")
  //     contentAsString(home) must include ("Your new application is ready.")
  //   }
  //
  // }

}
