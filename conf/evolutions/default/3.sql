# --- !Ups

CREATE TABLE expens (
  id varchar NOT NULL,
  money int NOT NULL,
  created varchar NOT NULL,
  note varchar NOT NULL
);
ALTER TABLE expens ADD CONSTRAINT expens_id_pk PRIMARY KEY(id);

# --- !Downs

DROP TABLE expens;
