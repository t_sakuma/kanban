# --- !Ups

CREATE TABLE todo (
  id varchar NOT NULL,
  title varchar NOT NULL,
  status varchar NOT NULL
);
ALTER TABLE todo ADD CONSTRAINT todo_id_pk PRIMARY KEY(id);

# --- !Downs

DROP TABLE todo;
