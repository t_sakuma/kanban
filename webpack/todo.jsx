import React from 'react'
import 'whatwg-fetch'

export default React.createClass({
  getInitialState: function () {
    return {todos: []}
  },
  getTodo: function () {
    fetch('/api/todo')
      .then((response) => response.json())
      .then((json) => this.setState({todos: json}))
      .then(() => setTimeout(this.getTodo, 5000))
      .catch((ex) => console.log('parsing failed', ex))
  },
  componentDidMount: function() {
    this.getTodo()
  },
  render: function () {
    var todos = this.state.todos
    var todoListMapper = (todo) => (<div key={todo.id}>{todo.id},{todo.title},{todo.status}</div>)
    return (
      <div>{todos.map(todoListMapper)}</div>
    )
  }
})
