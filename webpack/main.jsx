import React from 'react'
import { render } from 'react-dom'
import TodoList from './todo.jsx'

function safeRender(data, id) {
  if (id) {render(data, id)}
}

safeRender(<TodoList />, document.getElementById('todo'))
safeRender(<TodoList />, document.getElementById('app'))
