package repository

import scala.concurrent.Future

import javax.inject.Inject
import models.Todo
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile

class TodoRepo @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile]
{
  import driver.api._

  private val todos = TableQuery[TodosTable]

  def nextId(): String = java.util.UUID.randomUUID.toString

  def all(): Future[Seq[Todo]] = db.run(todos.sortBy(todo => todo.title.asc).result)

  def find(id: String): Future[Option[Todo]] = db.run(todos.filter(_.id === id).result.headOption)

  def insert(todo: Todo): Future[Unit] = db.run((todos += todo).transactionally).map { _ => () }

  def update(id: String, updateFunc: Todo => Todo): Future[Unit] = db.run((for {
    todo <- todos.filter(_.id === id).result.head
    _ <- todos.filter(_.id === todo.id).update(updateFunc(todo))
  } yield()).transactionally).map { _ => () }

  private class TodosTable(tag: Tag) extends Table[Todo](tag, "TODO") {

    def id = column[String]("ID", O.PrimaryKey)
    def title = column[String]("TITLE")
    def status = column[String]("STATUS")

    def * = (id, title, status) <> ((Todo.apply _).tupled, Todo.unapply _)
  }
}
