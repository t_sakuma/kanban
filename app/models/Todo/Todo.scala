package models

case class Todo(id: String, title: String, status: String = Todo.DO) {
  def setDone(): Todo = copy(status = Todo.DONE)
  def setDo(): Todo = copy(status = Todo.DO)
  def setDoing(): Todo = copy(status = Todo.DOING)
}

object Todo {
  // enum
  val DO = "do"
  val DOING = "doing"
  val DONE = "done"
}
