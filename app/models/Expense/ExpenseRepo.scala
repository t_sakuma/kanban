package repository

import scala.concurrent.Future

import javax.inject.Inject
import models._
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import slick.driver.JdbcProfile

class ExpenseRepo @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile]
{
  import driver.api._

  private val expenses = TableQuery[ExpenseTable]

  def insert(expense: Expense): Future[Unit] = db.run((expenses  += ExpenseRow(expense.accountId.id, expense.date.getMillis, expense.money)).transactionally) map {
    _ => ()
  }

  private case class ExpenseRow(accountId: String, timestamp: Long, money: Int)
  private class ExpenseTable(tag: Tag) extends Table[ExpenseRow](tag, "EXPENSE") {
    def accountId = column[String]("ACCOUNT_ID")
    def timestamp = column[Long]("TIMESTAMP")
    def money = column[Int]("MONEY")
    def * = (accountId, timestamp, money) <> ((ExpenseRow.apply _).tupled, ExpenseRow.unapply _)
  }
}
