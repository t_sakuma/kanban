package models

import org.joda.time.DateTime

case class ExpenseId(accountId: AccountId, date: DateTime)
case class Expense(id: ExpenseId, money: Int) {
  val accountId = id.accountId
  val date = id.date
}
