package models

import scala.concurrent._
import scala.concurrent.duration._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

object ExpenseFlow {
  def insert(
    expense: Expense,
    accountGet: AccountId => Future[Option[Account]],
    expenseInsert: Expense => Future[Unit]
  ): Future[Unit] = {
    accountGet(expense.accountId) flatMap { accountOpt =>
      if (accountOpt.isEmpty) throw new Exception("家計簿を取得できません")
      expenseInsert(expense)
    }
  }

  def ifAccountExist(
    accountOpt: Option[Account]
  ): Unit = {
    if (accountOpt.isEmpty) throw new Exception("家計簿を取得できません")
  }
}
