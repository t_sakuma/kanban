package repository

import scala.concurrent.Future

import javax.inject.Inject
import models._
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import org.joda.time.DateTime
import slick.driver.JdbcProfile

class EaiRepo @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile]
{
  import driver.api._

  private val eais = TableQuery[EaiTable]

  def nextId(): String = java.util.UUID.randomUUID.toString

  def nextVersion(id: String): Future[Long] = db.run(
    eais.filter(_.id === id).sortBy(eai => eai.version.desc).result.headOption
  ) map {
    _ match {
      case Some(eaiRow) => eaiRow.version + 1
      case None => 0
    }
  }

  def all(): Future[Seq[Eai]] = db.run(
    eais.sortBy(eai => eai.version.asc).result
  ) map {
    _.map { eaiRow => toEntity(eaiRow) }
  }

  def insert(eai: Eai): Future[Unit] = db.run(
    (eais += toRow(eai)).transactionally
  ).map { _ => () }

  def update(id: String, version: Long, updateFunc: Eai => Eai): Future[Unit] = {
    def compFunc(eaiRow: EaiRow): EaiRow = toRow(updateFunc(toEntity(eaiRow)))
    db.run((for {
      eaiRow <- eais.filter(_.id === id).filter(_.version === version).result.head
      _ <- eais.filter(_.id === eaiRow.id).filter(_.version === eaiRow.version)
        .update(compFunc(eaiRow))
    } yield()).transactionally).map { _ => () }
  }

  private case class EaiRow(id: String, version: Long, data: String)
  private class EaiTable(tag: Tag) extends Table[EaiRow](tag, "EAI") {
    def id = column[String]("ID")
    def version = column[Long]("VERSION")
    def data = column[String]("DATA")
    def * = (id, version, data) <> ((EaiRow.apply _).tupled, EaiRow.unapply _)
  }

  private def toEntity(eaiRow: EaiRow): Eai =
      Eai(
        eaiRow.id,
        eaiRow.version,
        (Json.parse(eaiRow.data) \ "eventType").as[String],
        new DateTime((Json.parse(eaiRow.data) \ "timestamp").as[Long]),
        (Json.parse(eaiRow.data) \ "monye").as[Int]
      )

  private def toRow(eai: Eai): EaiRow =
    EaiRow(eai.id, eai.version, Json.obj(
       "eventType" -> eai.eventType,
       "money" -> eai.money,
       "timestamp" -> eai.date.getMillis
    ).toString)

}
