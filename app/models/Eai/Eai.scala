package models

import org.joda.time.DateTime

case class Eai(id: String, version: Long, eventType: String, date: DateTime, money: Int)
