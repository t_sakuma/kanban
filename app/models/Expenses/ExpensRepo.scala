package repository

import scala.concurrent.Future

import javax.inject.Inject
import models.Expens
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile

class ExpensRepo @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile]
{
  import driver.api._

  private val expenses = TableQuery[ExpensTable]

  def nextId(): String = java.util.UUID.randomUUID.toString

  def all(): Future[Seq[Expens]] = db.run(expenses.sortBy(expens => expens.created.asc).result)

  def find(id: String): Future[Option[Expens]] = db.run(expenses.filter(_.id === id).result.headOption)

  def insert(expens: Expens): Future[Unit] = db.run((expenses += expens).transactionally).map { _ => () }

  def update(id: String, updateFunc: Expens => Expens): Future[Unit] = db.run((for {
    expens <- expenses.filter(_.id === id).result.head
    _ <- expenses.filter(_.id === expens.id).update(updateFunc(expens))
  } yield()).transactionally).map { _ => () }

  private class ExpensTable(tag: Tag) extends Table[Expens](tag, "EXPENS") {

    def id = column[String]("ID", O.PrimaryKey)
    def money = column[Int]("MONEY")
    def created = column[String]("CREATED")
    def note = column[String]("NOTE")

    def * = (id, money, created, note) <> ((Expens.apply _).tupled, Expens.unapply _)
  }
}
