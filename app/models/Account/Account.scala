package models

case class AccountId(id: String)
case class Account(id: AccountId)
