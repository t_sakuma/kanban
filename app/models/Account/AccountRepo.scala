package repository

import scala.concurrent.Future

import javax.inject.Inject
import models._
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import slick.driver.JdbcProfile

class AccountRepo @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile]
{
  import driver.api._

  private val accounts = TableQuery[AccountTable]

  def findById(accountId: AccountId): Future[Option[Account]] = db.run(accounts.filter(_.id === accountId.id).result.headOption) map {
    _ match {
      case Some(a) => Option(Account(AccountId(a.id)))
      case None => None
    }
  }

  private case class AccountRow(id: String, dummy: String)
  private class AccountTable(tag: Tag) extends Table[AccountRow](tag, "ACCOUNT") {
    def id = column[String]("ID")
    def dummy = column[String]("ID")
    def * = (id, dummy) <> ((AccountRow.apply _).tupled, AccountRow.unapply _)
  }
}
