package repository

import scala.concurrent.Future

import javax.inject.Inject
import models._
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import slick.driver.JdbcProfile

class BalancingRepo @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)
  extends HasDatabaseConfigProvider[JdbcProfile]
{
  import driver.api._

  private val balancingeEvents = TableQuery[BalancingEventTable]

  def nextId(): String = java.util.UUID.randomUUID.toString

  def all(): Future[Seq[BalancingEvent]] = db.run(balancingeEvents.sortBy(balancing => balancing.version.asc).result) map {
    _.map { _ match {
        case event if (Json.parse(event.data) \ "type").get == "BalancingEarnedIncome" => EarnedIncome(event.id, event.version, 0, new org.joda.time.DateTime)
        case event if (Json.parse(event.data) \ "type").get == "BalancingToSpended" => ToSpended(event.id, event.version, 0, new org.joda.time.DateTime)
      }
    }
  }

  private def convert(balancing: BalancingEvent): BalancingEventRow = balancing match {
    case e: EarnedIncome => BalancingEventRow(e.id, e.version, Json.obj(
       "id" -> e.id,
       "version" -> e.version,
       "type" -> "BalancingEarnedIncome",
       "money" -> e.money,
       "date" -> e.date
    ).toString)
    case e: ToSpended => BalancingEventRow(e.id, e.version, Json.obj(
       "id" -> e.id,
       "version" -> e.version,
       "type" -> "BalancingToSpended",
       "money" -> e.money,
       "date" -> e.date
    ).toString)
  }

  def insert(balancing: BalancingEvent): Future[Unit] = db.run(
    (balancingeEvents += convert(balancing)).transactionally
  ).map { _ => () }

  private case class BalancingEventRow(id: String, version: Long, data: String)

  private class BalancingEventTable(tag: Tag) extends Table[BalancingEventRow](tag, "BALANCING") {

    def id = column[String]("ID")
    def version = column[Long]("VERSION")
    def data = column[String]("DATA")

    def * = (id, version, data) <> ((BalancingEventRow.apply _).tupled, BalancingEventRow.unapply _)
  }
}
