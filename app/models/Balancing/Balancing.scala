package models

import org.joda.time.DateTime

case class Balancing(id: String, money: Int = 0, version: Long = 0) {
  def earnIncome(money: Int, date: DateTime): (Balancing, BalancingEvent)
    = this & EarnedIncome(id, version + 1, money, date)

  def toSpend(money: Int, date: DateTime): (Balancing, BalancingEvent)
    = this & ToSpended(id, version + 1, money, date)

  def +(event: BalancingEvent): Balancing = event match {
    case e: EarnedIncome => copy(version = e.version, money = money + e.money)
    case e: ToSpended => copy(version = e.version, money = money - e.money)
    case _ => copy()
  }

  def &(event: BalancingEvent): (Balancing, BalancingEvent) = (this + event, event)
}

object Balancing {
}

trait BalancingEvent
case class EarnedIncome(id: String, version: Long, money: Int, date: DateTime) extends BalancingEvent
case class ToSpended(id: String, version: Long, money: Int, date: DateTime) extends BalancingEvent
