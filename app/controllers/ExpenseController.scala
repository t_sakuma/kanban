package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import controllers.services._
import models._
import repository._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import play.api.libs.json._

@Singleton
class ExpenseController @Inject()(accountRepo: AccountRepo, expenseRepo: ExpenseRepo) extends Controller {
  // def expense() = Action.async(parse.json) { request =>
  //   val expense = ExpenseConverter.fromRequest(request.body)
  //   ifConversionSucceeded(expense) {
  //     _ => accountRepo.findById(expense.accountId)
  //   } map ifAcountIsFinded {
  //     _ => expenseRepo.insert(expense)
  //   } map ifInsertionSucceeded {
  //     """{"result": "OK"}"""
  //   } onComplete {
  //     case Success(msg) => Ok(msg)
  //     case Failure(e) => Future(BadRequest(e.getMessage))
  //   }
  // }
}
