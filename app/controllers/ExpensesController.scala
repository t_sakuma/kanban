package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import repository.ExpensRepo
import controllers.services._
import models.Expens
import play.api.libs.concurrent.Execution.Implicits.defaultContext

@Singleton
class ExpensController @Inject()(expensRepo: ExpensRepo) extends Controller {
  def index() = Action.async {
    expensRepo.all().map {
      case expenses => Ok(ExpensService.toIndexData(expenses))
    }
  }

  def add(money: Int, created: String, noteOpt: Option[String]) = Action.async { _ =>
    expensRepo.insert(Expens(expensRepo.nextId, money, created, noteOpt.getOrElse(""))).map { _ =>
      Ok("OK")
    } recover {
      case _ => BadRequest("登録に失敗しました。")
    }
  }

  def view() = Action {
    Ok(views.html.Expens.index())
  }
}
