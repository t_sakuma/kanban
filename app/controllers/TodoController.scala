package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import repository.TodoRepo
import controllers.services._
import models.Todo
import play.api.libs.concurrent.Execution.Implicits.defaultContext

@Singleton
class TodoController @Inject()(todoRepo: TodoRepo) extends Controller {
  def index() = Action.async {
    todoRepo.all().map {
      case todos => Ok(TodoConverter.toIndexData(todos))
    }
  }

  def add(title: String) = Action.async { _ =>
    todoRepo.insert(Todo(todoRepo.nextId, title)).map { _ =>
      Ok("OK")
    } recover {
      case _ => BadRequest("登録に失敗しました。")
    }
  }

  def done(id: String) = Action.async { _ =>
    todoRepo.update(id, todo => todo.setDone()).map { _ =>
      Ok("OK")
    } recover {
      case _ => BadRequest("変更に失敗しました。")
    }
  }

  def view() = Action {
    Ok(views.html.Todo.index())
  }
}
