package controllers.services

object ExpenseResponseFactory {
  def createSuccess(): String = """{"result" : "OK"}"""
  def createExpenseAddError(): String = """{"result" : "ExpenseAddError", "msg" : "支出の登録に失敗しました"}"""
  def createAccountRequestError(): String = """{"result" : "AccountRequestError", "msg" : "家計簿の取得に失敗しました"}"""
}
