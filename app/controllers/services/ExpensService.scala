package controllers.services

import play.api.libs.json._
import models.Expens

object ExpensService {
  def toIndexData(expenses: Seq[Expens]): JsArray = {
    val mapper = (expens: Expens) => JsObject(Seq(
      "id" -> JsString(expens.id),
      "money" -> JsNumber(expens.money),
      "created" -> JsString(expens.created),
      "note" -> JsString(expens.note)
    ))
    val delimiter = ","
    JsArray(expenses.map(mapper))
  }
}
