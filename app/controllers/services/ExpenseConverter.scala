package controllers.services

import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import models._
import org.joda.time.DateTime

object ExpenseConverter {
  case class Request(accountId: String, money: Int, timestamp: Long)
  val requestReads = (
    (JsPath \ "accountId").read[String] and
    (JsPath \ "money").read[Int] and
    (JsPath \ "timestamp").read[Long]
  )(Request.apply _)

  def fromRequest(request: JsValue): Expense = {
    request.validate[Request](requestReads) match {
      case s: JsSuccess[Request] =>
        val accountId = AccountId(s.get.accountId)
        val datetime = new DateTime(s.get.timestamp)
        Expense(ExpenseId(accountId, datetime), s.get.money)
      case e: JsError => throw new Exception(Json.obj("result" -> "JsError", "msg" -> JsError.toJson(e)).toString)
    }
  }
}
