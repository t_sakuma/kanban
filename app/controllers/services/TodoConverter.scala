package controllers.services

import play.api.libs.json._
import models.Todo

object TodoConverter {
  def toIndexData(todos: Seq[Todo]): JsArray = {
    val mapper = (todo: Todo) => JsObject(Seq(
      "id" -> JsString(todo.id),
      "title" -> JsString(todo.title),
      "status" -> JsString(todo.status)
    ))
    val delimiter = ","
    JsArray(todos.map(mapper))
  }
}
